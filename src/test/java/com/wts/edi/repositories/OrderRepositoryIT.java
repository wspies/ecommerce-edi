package com.wts.edi.repositories;

import com.wts.edi.api.v1.mapper.OrderMapper;
import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.api.v1.model.OrderListDTO;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;
import com.wts.edi.services.OrderService;
import com.wts.edi.services.jpa.OrderServiceJpa;
import org.hibernate.sql.ordering.antlr.OrderByAliasResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class OrderRepositoryIT {

    @Autowired
    OrderRepository orderRepository;


    @Test
    void findByType() {

        // given
        Order order = new Order();
        order.setId(1L);
        order.setSupplier("QUANTORE");
        order.setType(OrderType.PURCHASE);
        order.setDropshipment(true);

        OrderServiceJpa orderService = new OrderServiceJpa(orderRepository,OrderMapper.INSTANCE);
        orderService.save(order);

        //when
        List<Order> orders = orderRepository.findByType(OrderType.PURCHASE);

        //then
        assertEquals(1, orders.size());
    }
    /*@Test
    void findBySupplier() {
    }*/
}