package com.wts.edi.controllers;

import com.wts.edi.api.v1.mapper.OrderMapper;
import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.controllers.v1.OrderController;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;
import com.wts.edi.services.OrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

class OrderControllerTest {

    OrderController orderController;

    @Mock
    OrderService orderService;

    OrderMapper orderMapper = OrderMapper.INSTANCE;

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {

        MockitoAnnotations.initMocks(this);
        orderController = new OrderController(orderService);
        mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
    }

    @Test
    void getOrders() throws Exception {

        // given
        Order order = new Order();
        order.setId(1L);
        order.setSupplier("QUANTORE");
        order.setType(OrderType.PURCHASE);
        order.setDropshipment(true);

        List<OrderDTO> orders = new ArrayList<OrderDTO>();
        orders.add(orderMapper.orderToOrderDto(order));

        //when
        when(orderService.findAll()).thenReturn(orders);

        //then
        mockMvc.perform(get("/api/v1/orders/")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders",hasSize(1)));

    }




    /*@Test
    void getIndexPage() {

        //given
        Set<Order> orders = new HashSet<>();
        orders.add(new Order());
        orders.add(new Order());

        when(orderService.getOrders()).thenReturn(orders);
        ArgumentCaptor<Set<Order>> argumentCaptor = ArgumentCaptor.forClass(Set.class);

        //when
        String viewName = orderController.GetIndexPage(model);

        //then
        assertEquals("index", viewName);
        verify(orderService,times(1)).getOrders();
        verify(model,times(1)).addAttribute(eq("orders"),argumentCaptor.capture());
        Set<Order> setInOrders = argumentCaptor.getValue();
        assertEquals(2,setInOrders.size());


    }*/
}