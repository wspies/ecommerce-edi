package com.wts.edi.api.v1.mapper;

import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class OrderMapperTest {

    public static final Long ID = 1L;
    public static OrderType TYPE = OrderType.PURCHASE;
    public static Boolean DROPSHIPMENT = true;
    public static String SUPPLIER = "QUANTORE";

    OrderMapper orderMapper = OrderMapper.INSTANCE;

    @Test
    void orderDto() throws Exception {

        // given
        Order order = new Order();
        order.setId(ID);
        order.setType(TYPE);
        order.setSupplier("QUANTORE");
        order.setDropshipment(DROPSHIPMENT);

        // when
        OrderDTO orderDTO = orderMapper.orderToOrderDto(order);

        // then
        assertEquals(ID, orderDTO.getId());
        assertEquals(TYPE.getValue(),orderDTO.getType());
        assertEquals(SUPPLIER, orderDTO.getSupplier());
        assertEquals(DROPSHIPMENT, orderDTO.isDropshipment());

    }
}