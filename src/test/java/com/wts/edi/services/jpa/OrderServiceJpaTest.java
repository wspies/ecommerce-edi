package com.wts.edi.services.jpa;

import com.wts.edi.api.v1.mapper.OrderMapper;
import com.wts.edi.api.v1.mapper.OrderMapperImpl;
import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;
import com.wts.edi.repositories.OrderRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OrderServiceJpaTest {

    OrderServiceJpa orderService;

    @Mock
    OrderRepository orderRepository;

    OrderMapper orderMapper = OrderMapper.INSTANCE;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);

        orderService = new OrderServiceJpa(orderRepository,orderMapper);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }


    @Test
    void findAll() {

        // given
        Order order = new Order();
        List<Order> orderData = new ArrayList<>();

        order.setId(1L);
        order.setSupplier("QUANTORE");
        order.setType(OrderType.PURCHASE);
        order.setDropshipment(true);
        orderData.add(order);

        when(orderRepository.findAll()).thenReturn(orderData);

        // when
        List<OrderDTO> orderSet = orderService.findAll();

        // then
        assertEquals(1,(orderSet).size());

        verify(orderRepository,times(1)).findAll();
    }


    @Test
    void findByType() {

        // given
        Order order = new Order();
        List<Order> orderData = new ArrayList<>();

        order.setId(1L);
        order.setSupplier("QUANTORE");
        order.setType(OrderType.PURCHASE);
        order.setDropshipment(true);

        orderData.add(order);

        when(orderRepository.findByType(OrderType.PURCHASE)).thenReturn(orderData);

        // when
        List<OrderDTO> orderSet = orderService.findByType(OrderType.PURCHASE);

        // then
        assertEquals(((List) orderSet).size(),1);
        verify(orderRepository,times(1)).findByType(OrderType.PURCHASE);
    }

    @Test
    void findBySupplier() {

        // given
        Order order = new Order();
        String supplier = "QUANTORE";
        List<Order> orderData = new ArrayList<>();

        order.setId(1L);
        order.setSupplier(supplier);
        order.setType(OrderType.PURCHASE);
        order.setDropshipment(true);

        orderData.add(order);

        when(orderRepository.findBySupplier(supplier)).thenReturn(orderData);

        // when
        List<OrderDTO> orderSet = orderService.findBySupplier(supplier);

        // then
        assertEquals(((List) orderSet).size(),1);
        verify(orderRepository,times(1)).findBySupplier(supplier);

    }
}
