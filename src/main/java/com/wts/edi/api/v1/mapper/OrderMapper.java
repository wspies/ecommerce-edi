package com.wts.edi.api.v1.mapper;

import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.model.Order;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface OrderMapper {

    OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);


    @Mapping(source = "type.value", target = "type")
    OrderDTO orderToOrderDto(Order order);

}
