package com.wts.edi.api.v1.model;

import lombok.Data;


@Data
public class OrderDTO {

    private Long id;
    private String supplier;
    private boolean dropshipment;
    private String type;
}
