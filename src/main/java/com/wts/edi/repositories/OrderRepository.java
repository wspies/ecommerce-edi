package com.wts.edi.repositories;

import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {

    List<Order> findByType(OrderType type);
    List<Order> findBySupplier(String supplier);
    }
