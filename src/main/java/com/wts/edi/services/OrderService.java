package com.wts.edi.services;

import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;

import java.util.List;

public interface OrderService  {

    OrderDTO findById(Long id);
    OrderDTO save(Order order);
    List<OrderDTO> findAll();
    List<OrderDTO> findByType(OrderType type);
    List<OrderDTO> findBySupplier(String supplier);
}
