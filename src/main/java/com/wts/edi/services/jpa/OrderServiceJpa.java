package com.wts.edi.services.jpa;

import com.wts.edi.api.v1.mapper.OrderMapper;
import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;
import com.wts.edi.repositories.OrderRepository;
import com.wts.edi.services.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.h2.util.New;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderServiceJpa implements OrderService {

    private final OrderRepository orderRespository;
    private final OrderMapper orderMapper;

    public OrderServiceJpa(OrderRepository orderRespository, OrderMapper orderMapper
    ) {

        this.orderRespository = orderRespository;
        this.orderMapper = orderMapper;
    }
    @Override
    public OrderDTO findById(Long id) {

        return orderRespository
                .findById(id)
                .map(orderMapper::orderToOrderDto)
                .orElse(null);
    }

    public OrderDTO save(Order order) {

        return orderMapper.orderToOrderDto(orderRespository.save(order));
    }

    public void delete(Order order){
        orderRespository.delete(order);
    }

    public void deleteById(Long id){
        orderRespository.deleteById(id);
    }

    @Override
    public List<OrderDTO> findAll(){

        return orderRespository.findAll()
                .stream()
                .map(orderMapper::orderToOrderDto)
                .collect(Collectors.toList());

    }

    @Override
    public List<OrderDTO> findByType(OrderType type){

        return orderRespository.findByType(type)
                .stream()
                .map(orderMapper::orderToOrderDto)
                .collect(Collectors.toList());

    }

    @Override
    public List<OrderDTO> findBySupplier(String supplier){

        return orderRespository.findBySupplier(supplier)
                .stream()
                .map(orderMapper::orderToOrderDto)
                .collect(Collectors.toList());

    }

}
