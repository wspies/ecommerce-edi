package com.wts.edi.controllers.v1;

import com.wts.edi.api.v1.model.OrderListDTO;
import com.wts.edi.services.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("api/v1/orders/")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {

        this.orderService = orderService;
    }

    @GetMapping
    public ResponseEntity<OrderListDTO> getOrders(){

        return new ResponseEntity<OrderListDTO>(
                new OrderListDTO(orderService.findAll()), HttpStatus.OK);
    }

}
