package com.wts.edi.bootstrap;

import com.wts.edi.api.v1.model.OrderDTO;
import com.wts.edi.model.Address;
import com.wts.edi.model.AddressType;
import com.wts.edi.model.Order;
import com.wts.edi.model.OrderType;
import com.wts.edi.services.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Component
public class DataLoader implements ApplicationRunner {

    private final OrderService orderService;

    public DataLoader(OrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        loadData();
    }

    private void loadData(){

        Order purchaseOrder = new Order();
        Address buyerAddress = new Address();
        Address deliveryAddress = new Address();
        Address invoiceAddress = new Address();

        purchaseOrder.setType(OrderType.PURCHASE);
        purchaseOrder.setSupplier("QUANTORE");
        purchaseOrder.setNote("test notitie");
        purchaseOrder.setExternalOrderId("1234566789");
        purchaseOrder.setDropshipment(true);
        purchaseOrder.setProcessDate(new Date());

       /* buyerAddress.setType(AddressType.BUYER);
        buyerAddress.setAddress("591-4149 Ullamcorper Ave");
        buyerAddress.setCity("La Magdelein");
        buyerAddress.setZipcode("5084");
        buyerAddress.setCountry("US");


        deliveryAddress.setType(AddressType.DELIVERY);
        deliveryAddress.setAddress("P.O. Box 374, 5557 Nullam Av.");
        deliveryAddress.setCity("San Damiano al Colle");
        deliveryAddress.setZipcode("53834-876");
        deliveryAddress.setCountry("US");

        invoiceAddress.setType(AddressType.INVOICE);
        invoiceAddress.setAddress(" Ap #577-6680 Euismod Ave");
        invoiceAddress.setCity("Sankt Johann im Pongau");
        invoiceAddress.setZipcode("8373");
        invoiceAddress.setCountry("US");

        purchaseOrder.setBuyerAddress(deliveryAddress);
        purchaseOrder.setShipToAddress(deliveryAddress);
        purchaseOrder.setInvoiceAddress(invoiceAddress);*/

        OrderDTO orderDTO = orderService.save(purchaseOrder);
        System.out.println("saved 1 order!");
    }
}
