package com.wts.edi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name="trackers")
public class Tracker extends BaseEntity {

    private String carrier;
    private String code;
}
