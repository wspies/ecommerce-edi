package com.wts.edi.model;

import lombok.Getter;
import lombok.Setter;


import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name="addresses")
public class Address extends BaseEntity {

    private AddressType type;
    private String code;
    private String company;
    private String name;
    private String email;
    private String telephone;
    private String address;
    private String zipcode;
    private String city;
    private String state;
    private String country;
}
