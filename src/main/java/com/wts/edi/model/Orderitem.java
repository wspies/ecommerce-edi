package com.wts.edi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Setter
@Getter
public class Orderitem extends BaseEntity {

    @OneToOne
    private Order order;

    private String code;
    private String oemcode;
    private String name;
    private String currency;
    private String price;
    private int confirmed;
    private int delivered;
    private int cancelled;

    @Column(name="ship_date")
    private Date shipDate;

    @Column(name="delivery_date")
    private Date deliveryDate;

    private String notes;
    private int orderline;

}

