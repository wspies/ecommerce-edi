package com.wts.edi.model;

public enum OrderType {

    PURCHASE("Purchase"),
    BACKORDER("Backorder"),
    PACKAGE("Package");

    private final String value;

    private OrderType(String type){
        this.value = type;
    }

    public String getValue(){

        return value;
    }

}
