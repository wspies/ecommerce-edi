package com.wts.edi.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;


@Entity
@Getter
@Setter
@Table(name = "orders")
public class Order extends BaseEntity {

    @Enumerated(value = EnumType.STRING)
    @Column(name = "order_type")
    private OrderType type;

    private String supplier;

    private Boolean dropshipment;

    @Column(name = "external_order_id")
    private String externalOrderId;

    private String note;

    @Column(name = "create_date")
    private Date createDate;

    @Column(name = "process_date")
    private Date processDate;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Address> addresses;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Orderitem> items;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Tracker> trackers;

    public void setInvoiceAddress(Address invoiceAddress){
        addresses.add(invoiceAddress);
    }

    public void setShipToAddress(Address deliveryAddress){
        addresses.add(deliveryAddress);
    }

    public void setBuyerAddress(Address deliveryAddress){
        addresses.add(deliveryAddress);
    }



}
